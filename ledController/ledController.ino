/* Frank Duvendack
 * RGB LED CONTROLLER
 * Modified: 2020.12.25
 * Started: 2017.04.??
 */


/*CONFIGURATION*/
#define ENABLED  1
#define DISABLED 0

/*
 * DEBUG_MODE
 * Controls the delay time in between every color update.
 * STEP_DELAY_MS will be set to 50 when DEBUG_MODE is ENABLED
 * STEP_DELAY_MS will be set to 1250 when DEBUG_MODE is DISABLED (normal operation)
 *
 * Default: DISABLED
 */
#define DEBUG_MODE                    DISABLED

/**
 * DEBUG_PRINT_MODE_AND_COLOR
 * Print the current system mode and color (uncalibrated) to serial everytime the 
 * displayed color changes.
 *
 * Default: DISABLED
 */
#define DEBUG_PRINT_MODE_AND_COLOR    DISABLED

/**
 * PRINT_DELAY_TO_SERIAL
 * Print the delay between color changes (value from interpolation table)
 *
 * Default: DISABLED
 */
#define PRINT_DELAY_TO_SERIAL         DISABLED 

/*END CONFIGURATION*/

/*ENUM*/

/**
 * Enumeration to say whether a color should fade in or out
 */
enum class FadeDirection {
  IN,
  OUT
};

/**
 * Enumeration to say what the current system mode is
 */
enum class ColorMode {
  SPECTRUM,
  CONSTANT,
  DUAL_FADE
};

/*END ENUM*/


/* UNION */

/**
 * RGBColor_t
 * Store an RGB color
 * Allow raw access as an unsigned long or as individual channels
 */
typedef union {
  unsigned long rawColor;

  struct {
    byte B;
    byte G;
    byte R;
    byte _UNUSED;
  };
} RGBColor_t;
/* END UNION */


/*CONSTANTS*/

/*PINS*/
const byte GREEN_PIN = 11;
const byte RED_PIN   = 10;
const byte BLUE_PIN  = 9;
const byte OFF_PIN   = 8;
const byte ON_PIN    = 12;
/*END PINS*/

/*PULSE DELAYS*/
const unsigned int REMOTE_PULSE_DELAY_MS = 500; //how long (ms) to activate on or off button on remote control

#if DEBUG_MODE != DISABLED
  #warning DEBUG MODE IS ENABLED
  const unsigned int STEP_DELAY_MS = 10;
#else
  const unsigned int STEP_DELAY_MS = 1250;
#endif

/*END PULSE DELAYS*/


const unsigned short NUM_FADE_SEGMENTS = 6U;                       //number of fade segments (e.g.: blue to cyan, cyan to green, etc.)
const byte STEP                        = 1;                        //how much to change a color each time it is updated
const byte NIBBLES_PER_COLOR           = 6;                        //2 nibbles for red, green, and blue when reading color over usb
const unsigned short NUM_STEPS_MAX     = 255U * NUM_FADE_SEGMENTS; //maximum number of steps we can take in spectrum mode
const unsigned int STANDARD_DELAY      = 5U;                       //standard delay (ms) to use in 'delay' function

/* Control how fast we fade in between colors using an interpolation table.
 * The current element will be selected depending on which fade segment we're in.
 * This element will be biased against the next element of the table (wrap around if
 * the current element is the last element) and used to calculate the number of
 * milliseconds between steps. (See TimeTableGraph.ods)
 *
 * Fade from blue to cyan and slow down as the displayed color approaches cyan.
 * Fade from cyan to green and speed up as the displayed color approaches green.
 * Fade from green to yellow and speed up as the displayed color approaches yellow.
 * Fade from yellow to red and slow down as the displayed color approaches red.
 * Fade from red to purple and speed up as the displayed color approaches purple.
 * Fade from purple to blue and slow down as the displayed color approaches blue
*/
const double TIME_TABLE[NUM_FADE_SEGMENTS] = { 0.5, 1.0, 0.70, 0.25, 0.75, 0.40 };

/*COLOR CALIBRATIONS*/
/*These are used to compenaste for the red channel being the weakest*/
const double GREEN_CAL_MIN = 0.3F; //Range: [0..1]
const double BLUE_CAL_MIN  = 0.2F; //Range: [0..1]
const double RED_CAL_MIN   = 1.0F; //Range: [0..1]
/*END COLOR CALIBRATIONS*/


const byte ASCII_DIGIT_TO_HEX   = 48; //offset ascii '0' - '9' to 0x0 - 0x9
const byte ASCII_CAPITAL_TO_HEX = 55; //offset ascii 'A' - 'F' to 0xa - 0xf
const byte ASCII_LOWER_TO_HEX   = 87; //offset ascii 'a' - 'f' to 0xa - 0xf


/*END CONSTANTS*/


/*STATIC VARS*/
static bool           power_enabled_S; //TRUE if system power is enabled; FALSE otherwise
static ColorMode      system_mode_S;   //The current color mode
static unsigned short num_steps_S;     //Total number of steps we've taken  Range: [0..1530]

static unsigned short num_steps_dual_fade_total_S; //maximum possible number of steps to fade between 2 colors in dual fade mode
static unsigned short num_steps_dual_fade_S;       //current step number when fading between 2 colors in dual fade mode
static boolean dual_fade_primary_to_secondary_S;   //TRUE if fading from primary to secondary in dual fade mode; false otherwise

static RGBColor_t primary_color_S;     //Current color information
static RGBColor_t secondary_color_S;   //Secondary color to be used in dual fade mode
static RGBColor_t shutdown_color_S;    //Store the current color so it can be recalled in consant mode between rgboff and rgbon

static RGBColor_t primaryNoCal_color_S;   //uncalibrated version of the color being displayed in constant mode, or the uncalibrated version
                                          //of the primary color being displayed in dual fade mode.
static RGBColor_t secondaryNoCal_color_S; //uncalibrated version of the secondary color being displayed in dual fade mode.
/*END STATIC VARS*/


/*MACROS*/

/**
 * Determine which direction a channel needs to step in in order to reach a desired value
 * current     - the current value of the channel
 * destination - the desired value of the channel
 * return      - -STEP if the channel needs to fade out; STEP if the channel needs to fade in
 */
#define GET_DIRECTION(current, destination) ((current - destination >= 0) ? -STEP : STEP)

/*END MACROS*/

void setup() {
  pinMode(BLUE_PIN,  OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(RED_PIN,   OUTPUT);
  pinMode(OFF_PIN,   OUTPUT);
  pinMode(ON_PIN,    OUTPUT);

  //initialize global variables
  power_enabled_S  = true;
  system_mode_S    = ColorMode::SPECTRUM;

  //initial color BLUE
  primary_color_S.rawColor = 0x0000FFUL;
  shutdown_color_S.rawColor = primary_color_S.rawColor;

  //set initial constant color to BLUE
  num_steps_S = 0U;

  //set initial color blue
  digitalWrite(BLUE_PIN,  HIGH);
  digitalWrite(GREEN_PIN, LOW);
  digitalWrite(RED_PIN,   LOW);
  digitalWrite(OFF_PIN,   LOW);
  digitalWrite(ON_PIN,    LOW);
  
  Serial.begin(9600);
}

void loop() {
  updateStates(); //check if any commands have come over usb
  num_steps_S = 0;

  //only allow 'off' to be called once whenever power_enabled_S is set to false
  static bool allowPowerOff = true;
  
  if(power_enabled_S) {
    if(system_mode_S == ColorMode::SPECTRUM) {
      primary_color_S.rawColor = 0x0000FFUL;
      cycleBGR();
    } else if(system_mode_S == ColorMode::DUAL_FADE) {
      cycleDual();
    } else if(system_mode_S == ColorMode::CONSTANT) {
      /* do nothing - hold current color */
    }

    //allow 'off' to be called again once power_enabled_S has been set to false
    allowPowerOff = true;
  } else {
    if(allowPowerOff) {
      off();
      //disallow 'off' to be called until power_enabled_S is set to true and then set to false again
      allowPowerOff = false; 
    }
  }
}

/**
 * Fade all pins out
 */
static void off() {
  for(int i = 0; i < 255; i++) {
    if(primary_color_S.B > 0) analogWrite(BLUE_PIN,  --primary_color_S.B);
    if(primary_color_S.G > 0) analogWrite(GREEN_PIN, --primary_color_S.G);
    if(primary_color_S.R > 0) analogWrite(RED_PIN,   --primary_color_S.R);
    delay(STANDARD_DELAY);
  }
  
  digitalWrite(RED_PIN, LOW);
  digitalWrite(GREEN_PIN, LOW);
  digitalWrite(BLUE_PIN, LOW);
}

/**
 * Fade from current color (or off state) into a new color color
 * desiredColor - desired final color to ramp primary_color_S to
 */
static void on(RGBColor_t desiredColor) {
  int R_dir = GET_DIRECTION(primary_color_S.R, desiredColor.R); 
  int G_dir = GET_DIRECTION(primary_color_S.G, desiredColor.G); 
  int B_dir = GET_DIRECTION(primary_color_S.B, desiredColor.B); 

  bool R_reached = (primary_color_S.R == desiredColor.R);
  bool G_reached = (primary_color_S.G == desiredColor.G);
  bool B_reached = (primary_color_S.B == desiredColor.B);

  while(!R_reached || !G_reached || !B_reached) {

    //ramp primary_color channels towards the desired values.
    //stop ramping the channel once the target has been reached.
    if(desiredColor.R != primary_color_S.R)
      analogWrite(RED_PIN, primary_color_S.R += R_dir);
    else
      R_reached = true;

    if(desiredColor.G != primary_color_S.G)
      analogWrite(GREEN_PIN, primary_color_S.G += G_dir);
    else
      G_reached = true;

    if(desiredColor.B != primary_color_S.B)
      analogWrite(BLUE_PIN, primary_color_S.B += B_dir);
    else
      B_reached = true;

    delay(STANDARD_DELAY);
  }
}

/**
 * Control functions and modes
 * 
 * Codes sent over usb:
 * 0                                       : fade rgb out
 * 1                                       : fade rgb in (resumes mode (constant or spectrum) from before rgboff)
 * 2                                       : activate off button on remote
 * 3                                       : activate on button on remote
 * 4                                       : transition to spectrum mode
 * 4<6 digit hex color>                    : transition to constant mode and set color
 * 5<6 digit hex color><6 digit hex color> : transition to dual fade mode
 * ?                                       : print the current mode and uncalibrated color to serial
 *
 * Returns true if system_mode_S updated (this includes setting system_mode_S to the same thing it currently is); false otherwise
 */
static bool updateStates() {

  bool modeUpdated = false;

  if(Serial.available()) {
    byte tmp = Serial.read();
    delay(STANDARD_DELAY);

    switch(tmp) {
      case '0': //RGBOFF
      {
        power_enabled_S = false;

        //transition to spectrum mode if in dual fade mode
        if(system_mode_S == ColorMode::DUAL_FADE) {
          system_mode_S = ColorMode::SPECTRUM;
          modeUpdated = true;

          Serial.println("0: SPECTRUM");
        }

        shutdown_color_S.rawColor = primary_color_S.rawColor;
        
        Serial.println("0: RGBOFF"); 
        break;
      }
      case '1': //RGBON
      {
        const RGBColor_t BLUE = {.rawColor = 0x0000FFUL};
        on(BLUE); //start on blue

        system_mode_S = ColorMode::SPECTRUM;
        power_enabled_S = true;
        
        modeUpdated = true;

        Serial.println("1: RGBON");
        break;
      }
      case '2': //OFF
      {
        digitalWrite(OFF_PIN, HIGH);
        delay(REMOTE_PULSE_DELAY_MS);
        digitalWrite(OFF_PIN, LOW);

        clearSerialInputBuffer();
        Serial.println("2: OFF");
        break;
      }
      case '3': //ON
      {
        digitalWrite(ON_PIN, HIGH);
        delay(REMOTE_PULSE_DELAY_MS);
        digitalWrite(ON_PIN, LOW);

        Serial.println("3: ON");
        break;
      }
      case '4': //CONSTANT COLOR TOGGLE
      {
        byte* newnibbles = new byte[NIBBLES_PER_COLOR];
        bool validdata = readColorFromSerial(newnibbles);

        //correct number of bytes and all bytes are valid
        if(validdata) {
          system_mode_S = ColorMode::CONSTANT;

          //combine raw color buffer from 'readColorFromSerial' into individual channels
          RGBColor_t processedColor;
          processColor(newnibbles, processedColor);
          
          primaryNoCal_color_S.rawColor = processedColor.rawColor;

          //calibrate color to account for dim red channel
          calibrateColors(processedColor);

          //fade to the calibrated color
          on(processedColor);
          
          Serial.println("4: CONSTANT");

          shutdown_color_S.rawColor = primary_color_S.rawColor;
          power_enabled_S = true;
          modeUpdated = true;
        } else {
          Serial.println("4: REJECTED");
        }
        
        break;
      }
      case '5': //dual fade mode
      {
        //read in 2 hex colors from serial
        byte* nibbles_1 = new byte[NIBBLES_PER_COLOR];
        byte* nibbles_2 = new byte[NIBBLES_PER_COLOR]; 
        bool validdata_1 = readColorFromSerial(nibbles_1);
        bool validdata_2 = readColorFromSerial(nibbles_2);

        power_enabled_S = true;

        //transition to DUAL_FADE if both colors are valid
        if(validdata_1 && validdata_2) {

          //combine raw color buffers from 'readColorFromSerial' into individual channels
          RGBColor_t processedColor;
          processColor(nibbles_1, processedColor);
          processColor(nibbles_2, secondary_color_S);
          
          primaryNoCal_color_S.rawColor = processedColor.rawColor;
          secondaryNoCal_color_S.rawColor = secondary_color_S.rawColor;

          //calibrate colors to account for dim red channel
          calibrateColors(processedColor);         
          calibrateColors(secondary_color_S);

          //if the calibrated colors are the same, transition to constant mode instead of dual_fade mode
          if(processedColor.rawColor == secondary_color_S.rawColor) {
            system_mode_S = ColorMode::CONSTANT;
            Serial.println("4: CONSTANT");
          } else {
            system_mode_S = ColorMode::DUAL_FADE;
            Serial.println("5: DUAL_FADE");
          }

          on(processedColor); //will set primary_color_S to processedColor
          modeUpdated = true;

        } else {
          Serial.println("5: REJECTED");
        }

        break;
      }
      case '?':
      {
        printSysInfoToSerial();
      }
    }

    delay(1);

    clearSerialInputBuffer();
  }

  #if DEBUG_PRINT_MODE_AND_COLOR == ENABLED
    printSysInfoToSerial();
  #endif

  return modeUpdated;
}

/**
 * Clear serial input buffer
 */
static void clearSerialInputBuffer() {
  //clear input buffer
  while(Serial.available()) {
    Serial.read();
    delay(1);
  }
}

/**
 * Print the current mode and color to serial
 */
static void printSysInfoToSerial() {
  unsigned long currentColor;

  if(system_mode_S == ColorMode::SPECTRUM) {
    currentColor = primary_color_S.rawColor;
  } else if(system_mode_S == ColorMode::CONSTANT) {
    currentColor = primaryNoCal_color_S.rawColor;
  } else { //DUAL_FADE
    //we can figure out the current un-calibrated color in dual fade mode by interpolating between primaryNoCal_color_S and
    //secondaryNoCal_color_S using the percent complete between primary_color_S and secondary_color_S.
    double bias = ((double)num_steps_dual_fade_S / (double)num_steps_dual_fade_total_S);

    RGBColor_t tmpColor; 

    //bias towards either primary or secondary color depending on which direction we're fading.
    //bias primary if we're fading from primary to secondary
    //bias secondary if we're fading from secondary to primary
    if(dual_fade_primary_to_secondary_S) {
      tmpColor.R = (byte)((double)primaryNoCal_color_S.R * (1.0F - bias)) + ((double)secondaryNoCal_color_S.R * bias),
      tmpColor.G = (byte)((double)primaryNoCal_color_S.G * (1.0F - bias)) + ((double)secondaryNoCal_color_S.G * bias),
      tmpColor.B = (byte)((double)primaryNoCal_color_S.B * (1.0F - bias)) + ((double)secondaryNoCal_color_S.B * bias);
    } else {
      tmpColor.R = (byte)((double)primaryNoCal_color_S.R * (bias)) + ((double)secondaryNoCal_color_S.R * (1.0F - bias)),
      tmpColor.G = (byte)((double)primaryNoCal_color_S.G * (bias)) + ((double)secondaryNoCal_color_S.G * (1.0F - bias)),
      tmpColor.B = (byte)((double)primaryNoCal_color_S.B * (bias)) + ((double)secondaryNoCal_color_S.B * (1.0F - bias));
    }

    currentColor = tmpColor.rawColor;
  }

  //build a null terminated string containing the hex value of currentColor
  char colorStr[NIBBLES_PER_COLOR + 1] = "000000\0";

  //iterate over each nibble in currentColor and convert the nibble to an ASCII character
  //start at the right-most nibble so the string is RGB rather than BGR.
  //index 5 is the char before the '\0'; we don't want to overwrite this character.
  for(int i = NIBBLES_PER_COLOR - 1; i >= 0 && currentColor > 0UL; i--) {
    //extract rightmost nibble
    colorStr[i] = (char)(currentColor & 0xF);

    //convert to ASCII character
    if(colorStr[i] >= 0 && colorStr[i] <= 9)
      colorStr[i] += ASCII_DIGIT_TO_HEX;
    else
      colorStr[i] += ASCII_CAPITAL_TO_HEX;

    //pop rightmost nibble
    currentColor >>= 4UL;
  }

  //print current system mode and the current color
  switch(system_mode_S) {
    case ColorMode::SPECTRUM: 
      Serial.println("SPECTRUM: " + String(colorStr));
      break;
    case ColorMode::CONSTANT:
      Serial.println("CONSTANT: " + String(colorStr));
      break;
    default: //DUAL_FADE
      Serial.println("DUAL_FADE: " + String(colorStr));
      break;
  }
}

/**
 * Read an RGB hex color from serial
 * Parameters:
 * byte* databuff - buffer to be populated with a hex color
 *                  e.g.: "AB1234" -> { 0xA, 0xB, 0x1, 0x2, 0x3, 0x4 }
 *
 * Returns:
 * true if 6 bytes were read in and all of the bytes were ascii characters in [a-fA-F0-9]
 * false otherwise
 */
static bool readColorFromSerial(byte* databuff) {

  int bytect = 0;
  bool validdata = true;

  //read while accumulating bytes, all of the bytes so far have been valid, and serial is still available
  for(bytect = 0; bytect < NIBBLES_PER_COLOR && validdata && Serial.available(); bytect++) {
    byte newbyte = (byte)Serial.read(); //read 8-bit ascii character
    delay(1);
    
    //convert ascii 0-F to numeric value (e.g.: 'F' -> 0xF)
    if(newbyte >= '0' && newbyte <= '9')
      newbyte -= ASCII_DIGIT_TO_HEX;
    else if(newbyte >= 'A' && newbyte <= 'F')
      newbyte -= ASCII_CAPITAL_TO_HEX;
    else if(newbyte >= 'a' && newbyte <= 'f')
      newbyte -= ASCII_LOWER_TO_HEX;
    
    databuff[bytect] = newbyte;

    //check only valid data
    if(databuff[bytect] > 0xF)
      validdata = false;
  }

  return validdata && (bytect == NIBBLES_PER_COLOR);
}

/**
 * Process a color buffer from 'readColorFromSerial' and separate it into individual channels
 * byte* asciiColorBuffer - 6 element array of bytes laid out in the following format
 *          [0] -> red channel upper nibble (0x0 - 0xF)
 *          [1] -> red channel lower nibble (0x0 - 0xF)
 *          [2] -> green channel upper nibble (0x0 - 0xF)
 *          [3] -> green channel lower nibble (0x0 - 0xF)
 *          [4] -> blue channel upper nibble (0x0 - 0xF)
 *          [5] -> blue channel lower nibble (0x0 - 0xF)
 *
 * byte& r - reference to red channel
 * byte& g - reference to green channel
 * byte& b - reference to blue channel
 */
static void processColor(byte* asciiColorBuffer, RGBColor_t& processedColor) { //byte& r, byte& g, byte& b) {
  processedColor.R = (asciiColorBuffer[0] << 4) | asciiColorBuffer[1];
  processedColor.G = (asciiColorBuffer[2] << 4) | asciiColorBuffer[3];
  processedColor.B = (asciiColorBuffer[4] << 4) | asciiColorBuffer[5];
}

/**
 * Perform 1 complete cycle through the BGR color spectrum
 * Interupt if ASCII '0' is sent over serial or transition into CONSTANT color mode
 */
static void cycleBGR() {
  digitalWrite(BLUE_PIN,  HIGH);
  digitalWrite(GREEN_PIN, LOW);
  digitalWrite(RED_PIN,   LOW);

  /*fade blue->green->red->blue
    rely on '&&' short-circuiting so we only execute the next ramp if
    power was stil enabled at the end of the previous ramp*/
  rampChannel(primary_color_S.G, GREEN_PIN, FadeDirection::IN)  && //blue-green
  rampChannel(primary_color_S.B, BLUE_PIN,  FadeDirection::OUT) && //green
  rampChannel(primary_color_S.R, RED_PIN,   FadeDirection::IN)  && //green-red
  rampChannel(primary_color_S.G, GREEN_PIN, FadeDirection::OUT) && //red
  rampChannel(primary_color_S.B, BLUE_PIN,  FadeDirection::IN)  && //red-blue
  rampChannel(primary_color_S.R, RED_PIN,   FadeDirection::OUT);   //blue
}

/**
 * Fade a channel in or out.  Accounts for power state and system mode changing
 * 
 * channel     - reference to channel value [0..255]
 * channel_pin - pin to write channel to
 * direction   - fade the pin in or out
 * 
 * returns power_enabled_S
 */
static bool rampChannel(byte& channel, byte channel_pin, FadeDirection direction) {
  int amnt = ((direction == FadeDirection::IN) ? STEP : -STEP); //step forwards or backwards depending on desired direction
  int goal = ((direction == FadeDirection::IN) ? 255 : 0);      //expected channel value at end of ramp
  bool modechanged = false;

  while(channel != goal && power_enabled_S && system_mode_S == ColorMode::SPECTRUM && !modechanged) {
    channel += amnt;
    analogWrite(channel_pin, channel);
    delay(scaleDelay());
    modechanged = updateStates();
    num_steps_S += STEP;
  }

  return power_enabled_S && (system_mode_S == ColorMode::SPECTRUM) && !modechanged;
}


/**
 * Use TIME_TABLE to determine how many milliseconds the delay between steps should be.
 * Only active in SPECTRUM mode.
 *
 * returns an unsigned int within the range [0..STEP_DELAY_MS] based on how far into a cycle we are and the values in TIME_TABLE
 */
static unsigned int scaleDelay() {
  double percComplete = (double)num_steps_S / (double)NUM_STEPS_MAX;         //how close we are to completing a cycle
  double percCompleteOverall = percComplete * (double)NUM_FADE_SEGMENTS; //scale to 6 so we can index TIME_TABLE (6 elements)

  //get 2 consecutive elements from TIME_TABLE.  Wrap around to index 0 if val1 is the last element of TIME_TABLE.
  double val1 = TIME_TABLE[(int)percCompleteOverall];
  double val2 = TIME_TABLE[(int)(percCompleteOverall + 1.0F >= (double)NUM_FADE_SEGMENTS ? 0.0F : percCompleteOverall + 1.0F)];

  int stepsInBlock = num_steps_S % 255;

  //blend val1 and val2, favoring the one from whichever index percCompleteOverall is closer to
  double bias2 = (double)stepsInBlock / 255.0F; //bias2 is the percent complete of the current segment of fading (6 total segments)
  double bias1 = 1.0F - bias2;                  //bias1 + bias2 will always be equal to 1

  unsigned int calcdDelay = (unsigned int)((((bias1 * val1) + (bias2 * val2)) * STEP_DELAY_MS));

#if PRINT_DELAY_TO_SERIAL != DISABLED
  #warning PRINT_DELAY_TO_SERIAL IS ENABLED
  Serial.println(calcdDelay);
#endif

  //scale STEP_DELAY_MS by the average of the 2 biased percentages
  return calcdDelay;
}

/**
 * cycleDual
 * Cycle between two colors (stored in primary_color_S and secondary_color_S)
 */
static void cycleDual() {
  //fadeToTarget sets secondary_color_S to whatever the current color is, thus the arguments are the same for both calls

  dual_fade_primary_to_secondary_S = true; //fading from primary to secondary
  if(fadeToTarget(secondary_color_S)) {
    dual_fade_primary_to_secondary_S = false; //fading from secondary to primary
    fadeToTarget(secondary_color_S);
  }
}

/**
 * fadeToTarget
 * fade from the currently displayed color to a new color
 */
static bool fadeToTarget(RGBColor_t targetColor) {
  
  num_steps_dual_fade_S = 0U;
  
  //set color2 = current color
  secondary_color_S.rawColor = primary_color_S.rawColor;

  //move channels either backwards or forwards
  int R_dir = GET_DIRECTION(primary_color_S.R, targetColor.R);
  int G_dir = GET_DIRECTION(primary_color_S.G, targetColor.G);
  int B_dir = GET_DIRECTION(primary_color_S.B, targetColor.B);

  bool R_reached = (primary_color_S.R == targetColor.R);
  bool G_reached = (primary_color_S.G == targetColor.G);
  bool B_reached = (primary_color_S.B == targetColor.B);
  bool modeChanged = false;

  //all channels are changing at once, so the maximum number of steps will be the channel with the greatest difference
  //"max(delta R, delta G, delta B)"
  num_steps_dual_fade_total_S = max(max(abs((int)primary_color_S.R - (int)targetColor.R),
                                        abs((int)primary_color_S.G - (int)targetColor.G)),
                                    abs((int)primary_color_S.B - (int)targetColor.B));

  //loop until all channels have reached their targets or the mode has changed
  while(!(R_reached && G_reached && B_reached) && power_enabled_S && !modeChanged) {
     if(!R_reached) primary_color_S.R += R_dir;
     if(!G_reached) primary_color_S.G += G_dir;
     if(!B_reached) primary_color_S.B += B_dir;
     
     R_reached = (primary_color_S.R == targetColor.R);
     G_reached = (primary_color_S.G == targetColor.G);
     B_reached = (primary_color_S.B == targetColor.B);

     analogWrite(RED_PIN,   primary_color_S.R);
     analogWrite(GREEN_PIN, primary_color_S.G);
     analogWrite(BLUE_PIN,  primary_color_S.B);
     
     delay(STEP_DELAY_MS);
     modeChanged = updateStates();

     num_steps_dual_fade_S++;
  }

  return power_enabled_S && (system_mode_S == ColorMode::DUAL_FADE);
}

/** 
 * Compensate for dim red channel by reducing green and blue channels
 *  
 * Compensation is linear; the stronger the red channel is, the more blue and green
 * will be reduced.
 */
static void calibrateColors(RGBColor_t& colorToCalibrate) {
  //Red as a percentage [0..1]
  double redFactor = colorToCalibrate.R / 255.0F;

  //scale blue and green cals to be in the range [cal..1.0]
  double greenScale = 1 - ((1 - GREEN_CAL_MIN) * redFactor);
  double blueScale = 1 - ((1 - BLUE_CAL_MIN) * redFactor);

  //scale green and blue channels
  colorToCalibrate.G = (byte)((double)colorToCalibrate.G * greenScale);
  colorToCalibrate.B = (byte)((double)colorToCalibrate.B * blueScale);

  colorToCalibrate._UNUSED = 0U;
}

